import { createApp } from "vue";
import "./style.css";
import Render from "./render/render.vue";

createApp(Render).mount("#app");
