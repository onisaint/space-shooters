import { IGameState, IGameFrame } from "./interface";
import { Game } from "./main";

export function registerStateChange(
  this: Game,
  fn: (state: IGameState) => void,
): () => void {
  this.emitters.sateChange.set(fn, true);

  return () => this.emitters.sateChange.delete(fn);
}

export function emmitState(this: Game) {
  const state = this.getState();

  for (let fn of this.emitters.sateChange.keys()) {
    fn(state);
  }
}

export function registerFrameChange(
  this: Game,
  fn: (state: IGameFrame) => void,
) {
  this.emitters.frameChange.set(fn, true);

  return () => this.emitters.frameChange.delete(fn);
}

export function emmitFrame(this: Game) {
  const frame = this.getFrame();

  for (let fn of this.emitters.frameChange.keys()) {
    fn(frame);
  }
}
