export interface IGameBox {
  /** height of game window */
  boxHeight: number;
  /** width of game window */
  boxWidth: number;
}

export interface IGameState {
  state: "init" | "play" | "pause" | "over";
  level: number;
  score: number;
  health: number;
  maxHealth: number;
}

export type TArtifact = {
  id: string;
  top: number;
  left: number;
  isFallen?: boolean;
};

export interface IGameFrame {
  gun: TArtifact & { isHidden: boolean; coolDownFrame: number };
  bullets: (TArtifact & {
    isFriendly: boolean;
  })[];
  asteroids: TArtifact[];
  enemies: (TArtifact & { coolDownFrame: number })[];
  boom: (TArtifact & { removeAfterFrameCount: number; isPainful?: boolean })[];
  nextLevel: { level: number; removeAfterFrameCount: number };
}

export interface INextFrameAction {
  moveLeft: boolean;
  moveRight: boolean;
  fire: boolean;
}
