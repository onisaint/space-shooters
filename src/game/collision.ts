import { getRandom } from "./getRandom";
import { INextFrameAction, IGameFrame, TArtifact } from "./interface";
import { Game } from "./main";

const _HIT_BOX = 24;

function isInHitBox(a: TArtifact, b: TArtifact) {
  return (
    Math.abs(a.top - b.top) < _HIT_BOX && Math.abs(a.left - b.left) < _HIT_BOX
  );
}

function isDamaging(a: TArtifact, height: number) {
  return a.top > height - 40;
}

export function collision(
  this: Game,
  _: INextFrameAction,
  { height }: DOMRect,
  frame: IGameFrame,
): IGameFrame {
  const { boom } = frame;
  const friendlyBullets = frame.bullets.filter((bullet) => bullet.isFriendly);
  const fallenBulletIndex: number[] = [];

  let nextBullets = frame.bullets.filter((bullet) => !bullet.isFriendly);
  let nextAsteroids = frame.asteroids;
  let nextEnemies = frame.enemies;

  friendlyBullets.forEach((friend, i) => {
    // check for cancelling bullets
    nextBullets = nextBullets
      .map((bullet) => {
        if (isInHitBox(bullet, friend)) {
          fallenBulletIndex.push(i);
          return;
        }

        return bullet;
      })
      .filter(Boolean) as IGameFrame["bullets"];

    // boom asteroids
    nextAsteroids = nextAsteroids
      .map((asteroid) => {
        if (isInHitBox(asteroid, friend)) {
          fallenBulletIndex.push(i);
          boom.push({
            ...asteroid,
            removeAfterFrameCount: getRandom(100, 150),
          });
          this.score += 10;
          return;
        }

        return asteroid;
      })
      .filter(Boolean) as IGameFrame["asteroids"];

    // boom enemies
    nextEnemies = nextEnemies
      .map((enemy) => {
        if (isInHitBox(enemy, friend)) {
          fallenBulletIndex.push(i);
          boom.push({
            ...enemy,
            removeAfterFrameCount: getRandom(100, 150),
          });
          this.score += 20;
          return;
        }

        return enemy;
      })
      .filter(Boolean) as IGameFrame["enemies"];
  });

  let health = this.health;

  nextBullets = nextBullets
    .map((bullet) => {
      if (isDamaging(bullet, height)) {
        health -= 1;
        boom.push({
          ...bullet,
          removeAfterFrameCount: getRandom(100, 150),
          isPainful: true,
        });
        return;
      }

      return bullet;
    })
    .filter(Boolean) as IGameFrame["bullets"];

  nextAsteroids = nextAsteroids
    .map((asteroid) => {
      if (isDamaging(asteroid, height)) {
        health -= 2;
        boom.push({
          ...asteroid,
          removeAfterFrameCount: getRandom(100, 150),
          isPainful: true,
        });
        return;
      }

      return asteroid;
    })
    .filter(Boolean) as IGameFrame["asteroids"];

  this.health = Math.max(0, health);

  fallenBulletIndex.forEach((index) => friendlyBullets.splice(index, 1));

  const nextBoom = boom
    .map((boom) => {
      if (boom.removeAfterFrameCount > 0) {
        return {
          ...boom,
          removeAfterFrameCount: boom.removeAfterFrameCount - getRandom(1, 4),
        };
      }
    })
    .filter(Boolean) as IGameFrame["boom"];

  return {
    ...frame,
    bullets: [...nextBullets, ...friendlyBullets],
    asteroids: nextAsteroids,
    enemies: nextEnemies,
    boom: nextBoom,
  };
}
