import { Game } from "./main";

export function createScene(this: Game) {
  try {
    const scene = document.getElementById(this.frameId);
    if (scene) {
      scene.style.height = `${this.boxHeight}px`;
      scene.style.width = `${this.boxWidth}px`;
    }
  } catch (e) {
    console.log(e);
  }
}
