import { Game } from "./main";

const ARROW_LEFT = ["ArrowLeft", "KeyA"];
const ARROW_RIGHT = ["ArrowRight", "KeyD"];
const FIRE_PROJ = ["Space"];

export function handleKeyboardEvent(this: Game, e: KeyboardEvent) {
  const code = e.code;

  if (ARROW_LEFT.includes(code)) {
    this.nextFrameAction.moveLeft = true;
    return;
  }

  if (ARROW_RIGHT.includes(code)) {
    this.nextFrameAction.moveRight = true;
    return;
  }

  if (FIRE_PROJ.includes(code)) {
    this.nextFrameAction.fire = true;
    return;
  }
}
