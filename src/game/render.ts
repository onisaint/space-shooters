import { collision } from "./collision";
import { Game } from "./main";
import {
  getGunState,
  getBulletsStates,
  getAsteroidsState,
  getEnemitesState,
} from "./state";

function consumeFrame(this: Game) {
  this.nextFrameAction = {
    moveLeft: false,
    moveRight: false,
    fire: false,
  };
}

function makeNextFrame(this: Game) {
  const scene = document.getElementById(this.getFrameId());
  if (scene) {
    const frameRect = scene.getBoundingClientRect();

    const nextFrame = { ...this.nextFrameAction };
    consumeFrame.call(this);

    const frame = this.getFrame();

    const frameWithGunPos = getGunState.call(this, nextFrame, frameRect, frame);

    const frameWithAsteroids = getAsteroidsState.call(
      this,
      nextFrame,
      frameRect,
      frameWithGunPos,
    );

    const frameWithEnemites = getEnemitesState.call(
      this,
      nextFrame,
      frameRect,
      frameWithAsteroids,
    );

    const frameWithBullets = getBulletsStates.call(
      this,
      nextFrame,
      frameRect,
      frameWithEnemites,
    );

    const frameWithCollision = collision.call(
      this,
      nextFrame,
      frameRect,
      frameWithBullets,
    );

    this.frame = frameWithCollision;
  }
}

function updateGameState(this: Game) {
  const frame = this.frame;
  const level = Math.round(this.score / this.settings.levelProgress);

  if (level > this.level) {
    this.level = level;
    frame.nextLevel = {
      level,
      removeAfterFrameCount: 100,
    };
  }

  frame.nextLevel = {
    ...frame.nextLevel,
    removeAfterFrameCount: Math.max(
      0,
      frame.nextLevel.removeAfterFrameCount - 1,
    ),
  };

  if (this.health === 0) {
    this.state = "over";
  }

  this.frame = frame;
}

function animate(this: Game) {
  if (this.state === "pause" || this.state === "over") {
    cancelAnimationFrame(this.animationFrame);
    this.emmitState();
    return;
  }

  if (this.state === "play") {
    this.animationFrame = requestAnimationFrame(() => {
      animate.call(this);
    });
  }

  makeNextFrame.call(this);
  updateGameState.call(this);

  this.emmitState();
  this.emmitFrame();
}

export function renderScene(this: Game) {
  animate.call(this);
}
