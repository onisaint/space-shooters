import { flushFrame, pauseGame, playGame } from "./change";
import {
  emmitFrame,
  emmitState,
  registerFrameChange,
  registerStateChange,
} from "./emitter";
import { getBoxDimesions, getFrame, getFrameId, getState } from "./getters";
import { handleKeyboardEvent } from "./handler";
import {
  IGameFrame,
  IGameBox,
  INextFrameAction,
  IGameState,
} from "./interface";
import { renderScene } from "./render";
import { createScene } from "./scene";

class Game {
  boxHeight = 500;
  boxWidth = 400;

  frameId = "frame-";
  animationFrame = 0;

  state: IGameState["state"] = "init";
  maxHealth = 8;
  health = 8;
  level = 1;
  score = 0;

  settings = {
    enemiesInFrame: 2,
    enemiesSpanRate: 1,
    asteroidsInFrame: 2,
    asteroidsSpawnRate: 1,
    fireInterval: 0,
    bulletSpeed: 1,
    gunCoolDownSpeed: 4,
    enemiesCooldownSpeed: 1,
    levelProgress: 100,
  };

  frame: IGameFrame;

  nextFrameAction: INextFrameAction = {
    moveLeft: false,
    moveRight: false,
    fire: false,
  };

  emitters = {
    sateChange: new Map<Function, boolean>(),
    frameChange: new Map<Function, boolean>(),
  };

  constructor(box?: IGameBox) {
    this.boxHeight = box?.boxHeight ?? this.boxHeight;
    this.boxWidth = box?.boxWidth ?? this.boxWidth;
    this.frameId = `frame-${Date.now()}`;
    this.frame = flushFrame();
    this.emitters = {
      sateChange: new Map<Function, boolean>(),
      frameChange: new Map<Function, boolean>(),
    };
  }

  registerStateChange = registerStateChange.bind(this);
  emmitState = emmitState.bind(this);

  registerFrameChange = registerFrameChange.bind(this);
  emmitFrame = emmitFrame.bind(this);

  getState = getState.bind(this);
  getBoxDimesions = getBoxDimesions.bind(this);
  getFrameId = getFrameId.bind(this);
  getFrame = getFrame.bind(this);

  playGame = playGame.bind(this);
  pauseGame = pauseGame.bind(this);
  flushFrame = flushFrame.bind(this);

  handleKeyboard = handleKeyboardEvent.bind(this);

  createScene = createScene.bind(this);
  renderScene = renderScene.bind(this);
}

export { Game };
