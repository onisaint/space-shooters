import { IGameFrame, IGameBox, IGameState } from "./interface";
import { Game } from "./main";

export function getState(this: Game): IGameState {
  return {
    state: this.state,
    score: this.score,
    level: this.level,
    health: this.health,
    maxHealth: this.maxHealth,
  };
}

export function getBoxDimesions(this: Game): IGameBox {
  return {
    boxHeight: this.boxHeight,
    boxWidth: this.boxWidth,
  };
}

export function getFrameId(this: Game): string {
  return this.frameId;
}

export function getFrame(this: Game): IGameFrame {
  return this.frame;
}
