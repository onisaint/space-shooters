import { getRandom } from "./getRandom";
import { IGameFrame, INextFrameAction } from "./interface";
import { Game } from "./main";

export function getGunState(
  this: Game,
  nextFrame: INextFrameAction,
  { height, width }: DOMRect,
  frame: IGameFrame,
) {
  const { gun } = frame;
  let gunLeftPos = 0;

  if (!gun.isHidden) {
    gunLeftPos = gun.left;
    if (nextFrame.moveLeft) {
      gunLeftPos = Math.max(gunLeftPos - 24, 0);
    }
    if (nextFrame.moveRight) {
      gunLeftPos = Math.min(gunLeftPos + 24, width - 42);
    }
  }
  if (gun.isHidden) {
    gunLeftPos = width / 2 - 16;
    gun.isHidden = false;
  }

  gun.top = height - 40;
  gun.left = gunLeftPos;
  gun.coolDownFrame = Math.max(
    0,
    gun.coolDownFrame - this.settings.gunCoolDownSpeed,
  );

  return { ...frame, gun };
}

export function getBulletsStates(
  this: Game,
  nextFrame: INextFrameAction,
  { height }: DOMRect,
  frame: IGameFrame,
) {
  const { bullets, gun, enemies } = frame;

  const nextBullets = bullets
    .map((bullet) => {
      if (bullet.isFriendly) {
        const top = Math.max(0, bullet.top - this.settings.bulletSpeed);
        if (top) {
          return { ...bullet, top };
        }
      }

      if (!bullet.isFriendly) {
        const top = Math.min(height, bullet.top + this.settings.bulletSpeed);
        if (top) {
          return { ...bullet, top };
        }
      }
    })
    .filter(Boolean) as IGameFrame["bullets"];

  if (!gun.isHidden) {
    if (nextFrame.fire) {
      if (gun.coolDownFrame > 200) {
        gun.coolDownFrame = 700;
      }

      if (gun.coolDownFrame < 10) {
        nextBullets.push({
          id: `bullet-friendly-${nextBullets.length + 1}`,
          left: gun.left + 8,
          top: gun.top - 10,
          isFriendly: true,
        });

        gun.coolDownFrame = getRandom(400, 700);
      }

      if (gun.coolDownFrame < 150) {
        nextBullets.push(
          ...[
            {
              id: `bullet-friendly-${nextBullets.length + 1}`,
              left: gun.left + 8,
              top: gun.top - 16,
              isFriendly: true,
            },
            {
              id: `bullet-friendly-${nextBullets.length + 2}`,
              left: gun.left + 8,
              top: gun.top - 32,
              isFriendly: true,
            },
          ],
        );

        gun.coolDownFrame = getRandom(200, 400);
      }
    }
  }

  enemies.forEach((enemy) => {
    if (enemy.coolDownFrame === 0) {
      if (getRandom(0, 10000) < 2 * this.level) {
        nextBullets.push({
          id: `bullet-${nextBullets.length + 1}`,
          left: enemy.left + 8,
          top: enemy.top + 16,
          isFriendly: false,
        });
      }
    }
  });

  return { ...frame, bullets: nextBullets, gun };
}

export function getAsteroidsState(
  this: Game,
  _: INextFrameAction,
  { height, width }: DOMRect,
  frame: IGameFrame,
) {
  const { asteroids } = frame;

  if (
    asteroids.length <
    Math.floor(this.settings.asteroidsInFrame + this.level * 0.2)
  ) {
    if (getRandom(0, 1000) < 10) {
      asteroids.push({
        id: `asteroid-${asteroids.length + 1}`,
        top: getRandom(10, 15),
        left: getRandom(width / 4, width) - 40,
      });
    }
  }

  const nextAsteroids = asteroids
    .map((asteroid) => {
      const left = asteroid.left - 0.25;
      const top = asteroid.top + 0.5;
      if (left > 5 && top < height - 10) {
        return {
          ...asteroid,
          top,
          left,
        };
      }
    })
    .filter(Boolean) as IGameFrame["asteroids"];

  return { ...frame, asteroids: nextAsteroids };
}

export function getEnemitesState(
  this: Game,
  _: INextFrameAction,
  { width, height }: DOMRect,
  frame: IGameFrame,
) {
  const { enemies } = frame;

  if (enemies.length < this.settings.enemiesInFrame) {
    if (getRandom(0, 1000) < 10) {
      enemies.push({
        id: `enemies-${enemies.length + 1}`,
        top: getRandom(10, height / 2),
        left: width - 60,
        coolDownFrame: getRandom(0, 100),
      });
    }
  }

  const nextEnemies = enemies
    .map((enemy) => {
      const left = Math.max(5, enemy.left + getRandom(-1, 1));
      if (left > 10) {
        return {
          ...enemy,
          left,
          coolDownFrame: Math.max(
            0,
            enemy.coolDownFrame - this.settings.enemiesCooldownSpeed,
          ),
        };
      }
    })
    .filter(Boolean) as IGameFrame["enemies"];

  return { ...frame, enemies: nextEnemies };
}
