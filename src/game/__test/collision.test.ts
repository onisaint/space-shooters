import { describe, beforeEach, it, vi, expect } from "vitest";
import { Game } from "../main";

describe("game collision", () => {
  let currentGame: Game;

  beforeEach(() => {
    currentGame = new Game();
    const div = document.createElement("div");
    div.id = currentGame.getFrameId();
    div.getBoundingClientRect = vi.fn().mockReturnValue({
      height: currentGame.boxHeight,
      width: currentGame.boxWidth,
    });

    document.body.appendChild(div);
  });

  it("bullets in proximity cancel each other", async () => {
    currentGame.playGame();
    currentGame.pauseGame();

    currentGame.frame = {
      ...currentGame.frame,
      bullets: [
        { top: 40, left: 40, id: "1", isFriendly: true },
        { top: 44, left: 50, id: "2", isFriendly: false },
      ],
    };

    currentGame.playGame();

    await vi.waitFor(() => new Promise((r) => setTimeout(r, 200)));
    currentGame.pauseGame();

    expect(currentGame.getFrame().bullets.length).toBe(0);
  });

  it("bullets in far proximity doesn't cancel each other", async () => {
    currentGame.playGame();
    currentGame.pauseGame();

    currentGame.frame = {
      ...currentGame.frame,
      bullets: [
        { top: 40, left: 40, id: "1", isFriendly: true },
        { top: 80, left: 80, id: "2", isFriendly: false },
      ],
    };

    currentGame.playGame();

    await vi.waitFor(() => new Promise((r) => setTimeout(r, 200)));
    currentGame.pauseGame();

    expect(currentGame.getFrame().bullets.length).toBe(2);
  });

  it("destroying a asteroid increases score by 10", async () => {
    currentGame.playGame();
    currentGame.pauseGame();
    const prevScore = currentGame.getState().score;

    currentGame.frame = {
      ...currentGame.frame,
      asteroids: [{ top: 44, left: 44, id: "1" }],
      bullets: [{ top: 40, left: 40, id: "1", isFriendly: true }],
    };

    currentGame.playGame();

    await vi.waitFor(() => new Promise((r) => setTimeout(r, 200)));
    currentGame.pauseGame();

    expect(currentGame.getState().score - prevScore).toBe(10);
  });

  it("destroying a enemy increases score by 20", async () => {
    currentGame.playGame();
    currentGame.pauseGame();
    const prevScore = currentGame.getState().score;

    currentGame.frame = {
      ...currentGame.frame,
      enemies: [{ top: 44, left: 44, id: "1", coolDownFrame: 0 }],
      bullets: [{ top: 40, left: 40, id: "1", isFriendly: true }],
    };

    currentGame.playGame();

    await vi.waitFor(() => new Promise((r) => setTimeout(r, 200)));
    currentGame.pauseGame();

    expect(currentGame.getState().score - prevScore).toBe(20);
  });

  it("bullets hit reduces 1 health", async () => {
    currentGame.playGame();
    currentGame.pauseGame();
    const prevHealth = currentGame.getState().health;

    currentGame.frame = {
      ...currentGame.frame,
      bullets: [{ top: 460, left: 40, id: "1", isFriendly: false }],
    };

    currentGame.playGame();

    await vi.waitFor(() => new Promise((r) => setTimeout(r, 200)));
    currentGame.pauseGame();

    expect(prevHealth - currentGame.getState().health).toBe(1);
  });

  it("asteroid hit reduces 2 health", async () => {
    currentGame.playGame();
    currentGame.pauseGame();
    const prevHealth = currentGame.getState().health;

    currentGame.frame = {
      ...currentGame.frame,
      asteroids: [{ top: 460, left: 40, id: "1" }],
    };

    currentGame.playGame();

    await vi.waitFor(() => new Promise((r) => setTimeout(r, 200)));
    currentGame.pauseGame();

    expect(prevHealth - currentGame.getState().health).toBe(2);
  });
});
