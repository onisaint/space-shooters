import { IGameFrame } from "./interface";
import { Game } from "./main";

export function playGame(this: Game) {
  if (this.state !== "pause") {
    this.maxHealth = 8;
    this.health = 8;
    this.level = 1;
    this.score = 0;

    this.frame = flushFrame();
  }

  this.state = "play";

  this.emmitState();
  this.renderScene();
}

export function pauseGame(this: Game) {
  this.state = "pause";
  this.emmitState();
}

export function flushFrame(): IGameFrame {
  return {
    gun: {
      id: "gun",
      left: 0,
      top: 0,
      isHidden: true,
      coolDownFrame: 0,
    },
    bullets: [],
    asteroids: [],
    enemies: [],
    boom: [],
    nextLevel: {
      level: 1,
      removeAfterFrameCount: 0,
    },
  };
}
